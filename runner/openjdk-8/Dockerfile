# #####################################################################
# Build stage for running the Executable SpringBoot WAR files
#
# 1. Use a FROM gradle:4.10.1-jdk8 as unmazedboot-builder-artifacts
# 2. Build your steps needed
# 3. For runtime image use FROM marcellodesales/spring-boot-package-runner
#    - It will copy the generate WAR and resources properly
# * Look at background info https://spring.io/guides/gs/spring-boot-docker/
# * https://github.com/moby/moby/issues/15025#issuecomment-371466934
# #####################################################################
FROM openjdk:8-jre-slim

# ca certificates: to support HTTPS calls to other services
RUN apt-get update && apt-get install -y curl ca-certificates
RUN update-ca-certificates && mkdir -p /usr/share/ssl/certs && \
    chmod 755 /usr/share/ssl/certs

# libapr: to support Tomcat production http://tomcat.apache.org/native-doc/
# https://stackoverflow.com/questions/4278047/apr-based-apache-tomcat-native-library-was-not-found-on-the-java-library-path
# RUN apt-get install -y add-apt-repository
# RUN add-apt-repository ppa:ondrej/apache2 && apt-get update && apt-get install -y libapr1.0-dev libssl-dev
# https://stackoverflow.com/questions/40319869/spring-boot-embedded-tomcat-performance/40446766#40446766
# Change SpringBoot App to use APR: https://gist.github.com/andreldm/7f89a3279438467a0bd41e6c1249d014#file-aprconfiguration-java

WORKDIR /runtime

ONBUILD ARG RUNNER_EXTENSION
ONBUILD ENV RUNNER_EXTENSION=${RUNNER_EXTENSION}

ONBUILD ARG RUNNER_DIR
ONBUILD ENV RUNNER_DIR=${RUNNER_DIR}

ONBUILD ARG RUNNER_PORT
ONBUILD ENV RUNNER_PORT=${RUNNER_PORT}

# All files under RUNNER_HOOKS_DIR_SOURCES will be sourced
ONBUILD ARG RUNNER_HOOKS_DIR_SOURCES
ONBUILD ENV RUNNER_HOOKS_DIR_SOURCES=${RUNNER_HOOKS_DIR_SOURCES:-/runtime/sources}
ONBUILD RUN mkdir ${RUNNER_HOOKS_DIR_SOURCES}
# Creating the hook for environments to be sourced
ONBUILD RUN echo "export SPRINGBOOT_RUNNER_JRE_VERSION=\"openjdk:8-jre-slim\"" >> ${RUNNER_HOOKS_DIR_SOURCES}/springboot.sh && \
            echo "export SPRINGBOOT_RUNNER_START_TIME=\"$(date)\"" >> ${RUNNER_HOOKS_DIR_SOURCES}/springboot.sh

ONBUILD RUN echo "Sources hooks dir during build" && ls -la ${RUNNER_HOOKS_DIR_SOURCES}

# All files under RUNNER_HOOKS_ENV_VAR_DIR will be concatenated with JAVA_OPTS
ONBUILD ARG RUNNER_HOOKS_ENV_VAR_DIR
ONBUILD ENV RUNNER_HOOKS_ENV_VAR_DIR=${RUNNER_HOOKS_ENV_VAR_DIR:-/runtime/java-opts}
ONBUILD RUN echo "Creating java-opts hooks dir ${RUNNER_HOOKS_ENV_VAR_DIR} for JAVA_OPTS creation"
ONBUILD RUN mkdir ${RUNNER_HOOKS_ENV_VAR_DIR}
# Creating the hook for springboot to to be appended by entry.sh
ONBUILD RUN echo "-Djava.security.egd=file:/dev/./urandom" > ${RUNNER_HOOKS_ENV_VAR_DIR}/springboot.opt

# https://hub.docker.com/_/openjdk/, section "Make JVM respect CPU and RAM limits"
# On startup JVM tries to detect the number of available CPU cores and the amount of RAM
# to adjust its internal parameters (like the number of garbage collector threads to
# spawn) accordingly. When container is run with limited CPU/RAM, standard system API,
# used by JVM for probing, will return host-wide values. This can cause excessive CPU
# usage and memory allocation errors with older versions of JVM.
# Inside Linux containers, recent versions of OpenJDK 8 can correctly detect
# container-limited number of CPU cores by default. To enable the detection of
# container-limited amount of RAM the following options can be used:
ONBUILD RUN echo "-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap" > ${RUNNER_HOOKS_ENV_VAR_DIR}/docker.opt

# start.sh will execute CMD_ENV after processing the hooks dir and creating JAVA_OPTS
ONBUILD RUN echo "JAVA_OPTS hooks during build" && ls -la ${RUNNER_HOOKS_ENV_VAR_DIR}

# Copy from the previous stage
ONBUILD RUN echo "Will copy from unmazedboot-builder-artifacts /app/${RUNNER_DIR}/*.${RUNNER_EXTENSION} /tmp"
ONBUILD COPY --from=unmazedboot-builder-artifacts /app/${RUNNER_DIR}/ /tmp
ONBUILD RUN ls -la /tmp/*.${RUNNER_EXTENSION}

ONBUILD RUN echo "Will copy from /app/src/main/resources /runtime/resources"
ONBUILD COPY --from=unmazedboot-builder-artifacts /app/src/main/resources /runtime/resources
ONBUILD RUN ls -la /runtime/resources

# Just rename the built version
ONBUILD RUN echo "Renaming the executable ${RUNNER_EXTENSION} to the runtime dir"
ONBUILD RUN find /tmp -name "*.${RUNNER_EXTENSION}" ! -name "*sources*" ! -name "*javadoc*" -exec cp -t /runtime {} + && \
   mv /runtime/*.${RUNNER_EXTENSION} /runtime/server.jar && \
   rm -rf /tmp/*

ONBUILD EXPOSE ${RUNNER_PORT}

# The CMD exec can be provided by users as well, so users can change what is executed
ONBUILD ARG RUNNER_CMD_EXEC
ONBUILD ENV RUNNER_CMD_EXEC=${RUNNER_CMD_EXEC:-"java \$JAVA_OPTS -jar /runtime/server.jar \$JAR_OPTS"}
ONBUILD RUN echo "Defining the RUNNER_CMD_EXEC=${RUNNER_CMD_EXEC}. Override if needed."
ONBUILD RUN echo "Entrypoint will execute RUNNER_CMD_EXEC from /runtime/start.sh"

ADD start.sh /runtime/start.sh

# Start will call the RUNNER_CMD_EXEC value from the image after processing
# All the env hooks and JAVA_OPTS
ENTRYPOINT ["/bin/bash", "/runtime/start.sh"]
