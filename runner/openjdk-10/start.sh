#!/bin/bash

#####
## * Processes hook sources before
## * Creates ENV var
## * Calls CMD
##
## Author: Marcello.deSales@gmail.com
#####

# Source all env files in the

echo ""
echo "=> Initializing SpringBoot Runner 'start.sh'"
echo ""

if [ -z "${RUNNER_HOOKS_DIR_SOURCES}" ]; then
  RUNNER_HOOKS_DIR_SOURCES=/runtime/sources
fi

if [ -d "${RUNNER_HOOKS_DIR_SOURCES}" ]; then
  echo "=> Processing env hooks at ${RUNNER_HOOKS_DIR_SOURCES}"
  echo ""

  # Look for all env files
  FILE_NAMES=$(find ${RUNNER_HOOKS_DIR_SOURCES} -name '*.sh')
  FILES=(${FILE_NAMES// / })

  for i in "${!FILES[@]}"; do
    FILE=${FILES[i]}
    echo "[$((i+1))] source ${FILE}"
    source $FILE
  done
fi

# Create an environment hook to be created based on opt files
# This is here in case someone wants to reuse this for other languages
if [ -z "${RUNNER_ENV_HOOK_VAR}" ]; then
  RUNNER_ENV_HOOK_VAR=JAVA_OPTS
fi

if [ -z "${RUNNER_HOOKS_ENV_VAR_DIR}" ]; then
  RUNNER_HOOKS_ENV_VAR_DIR=/runtime/java-opts
fi

if [ -d "${RUNNER_HOOKS_ENV_VAR_DIR}" ]; then
  echo ""
  echo "=> Processing ${ENV_HOOK_VAR} hooks at ${RUNNER_HOOKS_ENV_VAR_DIR}"
  echo ""

  # Look for all opt files
  FILE_NAMES=$(find ${RUNNER_HOOKS_ENV_VAR_DIR} -name '*.opt')
  FILES=(${FILE_NAMES// / })

  # Concat the options from all the files
  OPTS=""
  for i in "${!FILES[@]}"; do
    FILE=${FILES[i]}
    echo "[$((i+1))] ${ENV_HOOK_VAR} << ${FILE}"
    OPTS="${OPTS} $(cat ${FILE})"
  done
  echo ""

  # https://askubuntu.com/questions/879144/how-to-generate-environment-variable-name-dynamically-and-export-it/879147#879147
  # https://stackoverflow.com/questions/11966983/bash-giving-a-printf-v-invalid-option-error/11967145#11967145
  echo "Exporting ${RUNNER_ENV_HOOK_VAR}=${OPTS}"
  export "${RUNNER_ENV_HOOK_VAR}=$(printf %s "${OPTS}")"
fi

echo ""

# Just print if it is in debug mode
if [ ! -z "${DEBUG_ENV}" ]; then
  echo "####### Debugging env before app start ##########"
  echo ""
  env
  echo ""
fi

echo "Starting the app "
echo "${RUNNER_CMD_EXEC}"
echo ""
sh -c "${RUNNER_CMD_EXEC}"
